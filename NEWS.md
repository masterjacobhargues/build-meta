# carbonOS 2021.1 (Alpha)

- Complete source-code restructuring
- Enable many different build-time security options and speed optimizations
- Remove debugging information from the binaries, making the OS much smaller
- Implement nsbox command environment
- Settings app
- Various fixes to the graphical session
- Switch carbonOS to btrfs
- Enable filesystem compression (reduce size of logs, cache, and the OS; increase drive longevity)
- Gracefully handle system OOM: swap-on-zram and systemd-oomd
- Replace sudo with doas
- Replace pulseaudio with pipewire
- Update boot animation to show carbonOS logo if no firmware logo is available
- Implement system updates service, CLI tool, and GUI plugin
- New Graphite login screen
- New installer and initial setup tool

# carbonOS 2020.1 (Pre-alpha)

- Initial release!

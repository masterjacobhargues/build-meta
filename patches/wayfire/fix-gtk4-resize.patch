From 1efdc1cff896f78c63f84e8eee8f46f6d659498d Mon Sep 17 00:00:00 2001
From: Adrian Vovk <adrianvovk@gmail.com>
Date: Mon, 16 Aug 2021 00:53:42 -0400
Subject: [PATCH] Implement missing functionality in gtk-shell

Fixes #1094
---
 src/output/gtk-shell.cpp | 169 ++++++++++++++++++++++++++++++++++-----
 1 file changed, 148 insertions(+), 21 deletions(-)

diff --git a/src/output/gtk-shell.cpp b/src/output/gtk-shell.cpp
index 1d47dbc17..904a20a34 100644
--- a/src/output/gtk-shell.cpp
+++ b/src/output/gtk-shell.cpp
@@ -15,6 +15,13 @@ struct wf_gtk_shell
     std::map<wl_resource*, std::string> surface_app_id;
 };

+struct wf_gtk_surface
+{
+    wl_resource *resource;
+    wl_resource *wl_surface;
+    wf::wl_listener_wrapper on_configure;
+    wf::wl_listener_wrapper on_destroy;
+};

 /**
  *  In gnome-shell/mutter/meta windows/views keep track of the properties
@@ -27,10 +34,11 @@ static void handle_gtk_surface_set_dbus_properties(wl_client *client,
     const char *window_object_path, const char *application_object_path,
     const char *unique_bus_name)
 {
-    auto surface = static_cast<wl_resource*>(wl_resource_get_user_data(resource));
+    auto surface = static_cast<wf_gtk_surface*>(wl_resource_get_user_data(resource));
     if (application_id)
     {
-        wf::get_core_impl().gtk_shell->surface_app_id[surface] = application_id;
+        wf::get_core_impl().gtk_shell->surface_app_id[surface->wl_surface] =
+            application_id;
     }
 }

@@ -42,8 +50,8 @@ static void handle_gtk_surface_set_dbus_properties(wl_client *client,
  */
 static void handle_gtk_surface_set_modal(wl_client *client, wl_resource *resource)
 {
-    auto surface = static_cast<wl_resource*>(wl_resource_get_user_data(resource));
-    wayfire_view view = wf::wl_surface_to_wayfire_view(surface);
+    auto surface = static_cast<wf_gtk_surface*>(wl_resource_get_user_data(resource));
+    wayfire_view view = wf::wl_surface_to_wayfire_view(surface->wl_surface);
     if (view)
     {
         view->store_data(std::make_unique<wf::custom_data_t>(), "gtk-shell-modal");
@@ -58,8 +66,8 @@ static void handle_gtk_surface_set_modal(wl_client *client, wl_resource *resourc
  */
 static void handle_gtk_surface_unset_modal(wl_client *client, wl_resource *resource)
 {
-    auto surface = static_cast<wl_resource*>(wl_resource_get_user_data(resource));
-    wayfire_view view = wf::wl_surface_to_wayfire_view(surface);
+    auto surface = static_cast<wf_gtk_surface*>(wl_resource_get_user_data(resource));
+    wayfire_view view = wf::wl_surface_to_wayfire_view(surface->wl_surface);
     if (view)
     {
         view->erase_data("gtk-shell-modal");
@@ -78,8 +86,8 @@ static void handle_gtk_surface_unset_modal(wl_client *client, wl_resource *resou
 static void handle_gtk_surface_present(wl_client *client, wl_resource *resource,
     uint32_t time)
 {
-    auto surface = static_cast<wl_resource*>(wl_resource_get_user_data(resource));
-    wayfire_view view = wf::wl_surface_to_wayfire_view(surface);
+    auto surface = static_cast<wf_gtk_surface*>(wl_resource_get_user_data(resource));
+    wayfire_view view = wf::wl_surface_to_wayfire_view(surface->wl_surface);
     if (view)
     {
         wf::view_focus_request_signal data;
@@ -100,8 +108,8 @@ static void handle_gtk_surface_request_focus(struct wl_client *client,
     struct wl_resource *resource,
     const char *startup_id)
 {
-    auto surface = static_cast<wl_resource*>(wl_resource_get_user_data(resource));
-    wayfire_view view = wf::wl_surface_to_wayfire_view(surface);
+    auto surface = static_cast<wf_gtk_surface*>(wl_resource_get_user_data(resource));
+    wayfire_view view = wf::wl_surface_to_wayfire_view(surface->wl_surface);
     if (view)
     {
         wf::view_focus_request_signal data;
@@ -112,12 +120,117 @@ static void handle_gtk_surface_request_focus(struct wl_client *client,
     }
 }

+/**
+ * Helper function used by send_gtk_surface_configure
+ * and send_gtk_surface_configure_edges
+ */
+static void append_to_array(wl_array *array, uint32_t value)
+{
+    uint32_t *tmp;
+    tmp  = (uint32_t*)wl_array_add(array, sizeof(*tmp));
+    *tmp = value;
+}
+
+/**
+ * Tells the client about the window state in more detail than xdg_surface.
+ * This currently only includes which edges are tiled.
+ */
+static void send_gtk_surface_configure(wf_gtk_surface *surface, wayfire_view view)
+{
+    int version = wl_resource_get_version(surface->resource);
+    wl_array states;
+    wl_array_init(&states);
+
+    if (view->tiled_edges)
+    {
+        append_to_array(&states, GTK_SURFACE1_STATE_TILED);
+    }
+
+    if ((version >= GTK_SURFACE1_STATE_TILED_TOP_SINCE_VERSION) &&
+        (view->tiled_edges & WLR_EDGE_TOP))
+    {
+        append_to_array(&states, GTK_SURFACE1_STATE_TILED_TOP);
+    }
+
+    if ((version >= GTK_SURFACE1_STATE_TILED_RIGHT_SINCE_VERSION) &&
+        (view->tiled_edges & WLR_EDGE_RIGHT))
+    {
+        append_to_array(&states, GTK_SURFACE1_STATE_TILED_RIGHT);
+    }
+
+    if ((version >= GTK_SURFACE1_STATE_TILED_BOTTOM_SINCE_VERSION) &&
+        (view->tiled_edges & WLR_EDGE_BOTTOM))
+    {
+        append_to_array(&states, GTK_SURFACE1_STATE_TILED_BOTTOM);
+    }
+
+    if ((version >= GTK_SURFACE1_STATE_TILED_LEFT_SINCE_VERSION) &&
+        (view->tiled_edges & WLR_EDGE_LEFT))
+    {
+        append_to_array(&states, GTK_SURFACE1_STATE_TILED_LEFT);
+    }
+
+    gtk_surface1_send_configure(surface->resource, &states);
+    wl_array_release(&states);
+}
+
+/**
+ * Tells gtk which edges should be resizable.
+ */
+static void send_gtk_surface_configure_edges(wf_gtk_surface *surface,
+    wayfire_view view)
+{
+    wl_array edges;
+    wl_array_init(&edges);
+
+    if (!view->tiled_edges)
+    {
+        append_to_array(&edges, GTK_SURFACE1_EDGE_CONSTRAINT_RESIZABLE_TOP);
+        append_to_array(&edges, GTK_SURFACE1_EDGE_CONSTRAINT_RESIZABLE_RIGHT);
+        append_to_array(&edges, GTK_SURFACE1_EDGE_CONSTRAINT_RESIZABLE_BOTTOM);
+        append_to_array(&edges, GTK_SURFACE1_EDGE_CONSTRAINT_RESIZABLE_LEFT);
+    }
+
+    gtk_surface1_send_configure_edges(surface->resource, &edges);
+    wl_array_release(&edges);
+}
+
+/**
+ * Augments xdg_surface's configure with additional gtk-specific information.
+ */
+static void handle_xdg_surface_on_configure(wf_gtk_surface *surface)
+{
+    wayfire_view view = wf::wl_surface_to_wayfire_view(surface->wl_surface);
+    if (view)
+    {
+        send_gtk_surface_configure(surface, view);
+        if (wl_resource_get_version(surface->resource) >=
+            GTK_SURFACE1_CONFIGURE_EDGES_SINCE_VERSION)
+        {
+            send_gtk_surface_configure_edges(surface, view);
+        }
+    }
+}
+
+/**
+ * Prevents a race condition where the xdg_surface is destroyed before
+ * the gtk_surface's resource and the gtk_surface's destructor tries to
+ * disconnect these signals which causes a use-after-free
+ */
+static void handle_xdg_surface_on_destroy(wf_gtk_surface *surface)
+{
+    surface->on_configure.disconnect();
+    surface->on_destroy.disconnect();
+}
+
 /**
  * Destroys the gtk_surface object.
  */
 static void handle_gtk_surface_destroy(wl_resource *resource)
-{}
-
+{
+    auto surface = static_cast<wf_gtk_surface*>(wl_resource_get_user_data(resource));
+    delete surface;
+}

 /**
  * Supported functions of the gtk_surface_interface implementation
@@ -131,16 +244,31 @@ const struct gtk_surface1_interface gtk_surface1_impl = {
 };

 /**
- * Passes the gtk_surface object to the client.
+ * Initializes a gtk_surface object and passes it to the client.
  */
 static void handle_gtk_shell_get_gtk_surface(wl_client *client,
     wl_resource *resource, uint32_t id,
     wl_resource *surface)
 {
-    auto res = wl_resource_create(client, &gtk_surface1_interface, wl_resource_get_version(
-        resource), id);
-    wl_resource_set_implementation(res, &gtk_surface1_impl, surface,
-        handle_gtk_surface_destroy);
+    wf_gtk_surface *gtk_surface = new wf_gtk_surface;
+    gtk_surface->resource = wl_resource_create(client, &gtk_surface1_interface,
+        wl_resource_get_version(resource), id);
+    gtk_surface->wl_surface = surface;
+    wl_resource_set_implementation(gtk_surface->resource, &gtk_surface1_impl,
+        gtk_surface, handle_gtk_surface_destroy);
+
+    wlr_surface *wlr_surface     = wlr_surface_from_resource(surface);
+    wlr_xdg_surface *xdg_surface = wlr_xdg_surface_from_wlr_surface(wlr_surface);
+    gtk_surface->on_configure.set_callback([=] (void*)
+    {
+        handle_xdg_surface_on_configure(gtk_surface);
+    });
+    gtk_surface->on_configure.connect(&xdg_surface->events.configure);
+    gtk_surface->on_destroy.set_callback([=] (void*)
+    {
+        handle_xdg_surface_on_destroy(gtk_surface);
+    });
+    gtk_surface->on_destroy.connect(&xdg_surface->events.destroy);
 }

 /**
@@ -168,7 +296,6 @@ static void handle_gtk_shell_set_startup_id(wl_client *client, wl_resource *reso
 /**
  *  A view could use this to invoke the system bell, be it aural, visual or none at
  * all.
- *  Not implemented.
  */
 static void handle_gtk_shell_system_bell(wl_client *client, wl_resource *resource,
     wl_resource *surface)
@@ -176,9 +303,9 @@ static void handle_gtk_shell_system_bell(wl_client *client, wl_resource *resourc
     wf::view_system_bell_signal data;
     if (surface)
     {
-        auto wl_surface =
-            static_cast<wl_resource*>(wl_resource_get_user_data(surface));
-        data.view = wf::wl_surface_to_wayfire_view(wl_surface);
+        auto gtk_surface =
+            static_cast<wf_gtk_surface*>(wl_resource_get_user_data(surface));
+        data.view = wf::wl_surface_to_wayfire_view(gtk_surface->wl_surface);
     }

     wf::get_core().emit_signal("view-system-bell", &data);

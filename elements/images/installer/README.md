This is the basic installer ISO file for carbonOS

Kernel: `generic`

Variant: `desktop`

Architectures: `x86_64`, `x86_64-v3`

Ships with Installer: yes

How to install: Just flash the ISO to a USB drive and boot off of it normally

Output file: `carbonOS-VERSION-installer.iso`

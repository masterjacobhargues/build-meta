kind: manual

# Entire setup for this horrible package taken from here:
# https://gitlab.com/freedesktop-sdk/freedesktop-sdk/blob/master/elements/components/nss.bst

sources:
- kind: git_tag
  url: github:nss-dev/nss
  track: NSS_3_69_BRANCH
  track-extra:
  - master
  match:
    - 'NSS_*_RTM'
  directory: nss
- kind: local
  path: files/nss-load-p11-kit-trust.c
# release-monitoring: 2503

depends:
- pkgs/nspr.bst
- pkgs/sqlite.bst
- pkgs/p11-kit.bst

build-depends:
- pkgs/perl/all.bst
- pkgs/gyp.bst
- buildsystems/autotools.bst

variables:
  build-args: >-
    --opt
    --system-nspr
    --system-sqlite
    --disable-tests
    --target=x64

environment:
  MAXJOBS: "%{max-jobs}"

environment-nocache:
- MAXJOBS

config:
  build-commands:
  - |
    cd nss
    ./build.sh -j "${MAXJOBS}" -v %{build-args}
  - |
    gcc ${CFLAGS} ${LDFLAGS} -Wall nss-load-p11-kit-trust.c -shared $(pkg-config --libs --cflags p11-kit-1) -o load-p11-kit-trust.so

  install-commands:
  - |
    cd dist
    install -Dm644 -t %{install-root}%{libdir} Release/lib/lib*.so
    rm %{install-root}%{libdir}/libnssckbi.so
    install -Dm755 -t %{install-root}%{bindir} Release/bin/*
    install -Dm644 -t %{install-root}%{includedir}/nss public/nss/*.h
  - |
    NSPR_VERSION="$(/usr/bin/nspr-config --version)"
    NSS_VERSION="$(grep NSS_VERSION dist/public/nss/nss.h | sed -e 's/[^"]*"//' -e 's/".*//' -e 's/ .*//')"
    MOD_MAJOR_VERSION="$(echo "${NSS_VERSION}" | cut -d. -f1)"
    MOD_MINOR_VERSION="$(echo "${NSS_VERSION}" | cut -d. -f2)"
    MOD_PATCH_VERSION="$(echo "${NSS_VERSION}" | cut -d. -f3)"

    for f in nss/pkg/pkg-config/*.in; do
      sed -f - "${f}" <<EOF >"$(basename "${f}" .in)"
    s,%,@,g
    s,@prefix@,%{prefix},g
    s,@exec_prefix@,%{exec_prefix},g
    s,@libdir@,%{libdir},g
    s,@includedir@,%{includedir}/nss,g
    s,@NSS_VERSION@,${NSS_VERSION},g
    s,@NSPR_VERSION@,${NSPR_VERSION},g
    s,@MOD_MAJOR_VERSION@,${MOD_MAJOR_VERSION},g
    s,@MOD_MINOR_VERSION@,${MOD_MINOR_VERSION},g
    s,@MOD_PATCH_VERSION@,${MOD_PATCH_VERSION},g
    EOF
    done
    install -m 644 -D -t %{install-root}/%{libdir}/pkgconfig nss.pc
    install -m 755 -D -t %{install-root}/%{bindir} nss-config

  - |
    install -Dm644 -t %{install-root}%{libdir}/ load-p11-kit-trust.so
    ln -s load-p11-kit-trust.so %{install-root}%{libdir}/libnssckbi.so

public:
  bst:
    split-rules:
      devel:
        (>):
        - "%{bindir}/*"

# Include the system-wide bashrc
. /etc/bashrc

# User sepecific path extension
if ! [[ "$PATH" =~ "$HOME/.local/bin" ]]; then
  PATH="$HOME/.local/bin:$PATH"
fi
export PATH

# User-specific aliases & functions

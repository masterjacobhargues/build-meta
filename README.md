# carbonOS

carbonOS is a Linux distribution designed from the ground-up to be robust,
modern, easy-to-use, and compatible with a wide range of software. Read more
[here](https://carbon.sh)

## Project structure

- `elements/`: [BuildStream](https://buildstream.build) elements that define the OS's components
    - `pkgs/`: Defines all of the individual packages that make up the OS
    - `bootstrap/`: The basic toolchain used to bootstrap the rest of the OS
    - `buildsystems/`: Convenient way to import all the dependencies for a standard build system (meson, autotools, cmake, etc)
    - `groups/`: The basic package groups that carbonOS is split into. Everything that is pulled into an image is in a group
    - `images/`: Target device image definitions
    - `kernels/`: Variants of the Linux kernel supported by carbonOS
    - `variants/`: Variants of the base OS
    - `tools/`: Elements that are there to help the build in some way and shouldn't be treated normally
- `files/`: Various auxiliary files that are part of carbonOS's build (i.e. default config)
- `include/`: YAML fragments that are reused in `elements/` and `project.conf`
- `keys/`: Standard location to put GPG keys for signing carbonOS. Ignored by git. See `keys/README.md` for more info
- `patches/`: Patches that get applied onto packages in `elements/pkgs/`. Ideally kept to a minimum
- `plugins/`: Custom BuildStream plugins that are used in `elements/`
    - `ostree*`: A buildstream plugin that exports files into an OSTree Repository. Written for [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/tree/master/plugins)
- `project.conf`: The BuildStream project configuration
- `*.refs`: Used by BuildStream to keep track of the versions of various components
- `pub/`: Staging directory for publishing. Ignored by git
- `result/`: Standard location that `tools/checkout` exports BuildStream artifacts to
- `tools/`: Helpful scripts for building carbonOS
    - `bumpver`: Increments carbonOS's version number
    - `checkout`: Retrieves compiled artifacts from BuildStream and puts them into `result/`
    - `update`: Comprehensive script that updates every single package in the repo. RUN WITH CARE
    - `test`: Simple way to stage a few elements together and run them in a shell
    - `setup-devrepo`: Create the structure in pub/ for a development OSTree repo
    - `publish`: Publish os/base/x86_64/desktop and os/kernel/x86_64/generic to the dev repo in pub

## Build instructions

#### Dependencies

First, you need to install build dependencies. If you are running carbonOS, this
is simple:
```bash
$ updatectl switch --base=devel
[Enter administrator password at the prompt]
[Reboot]
```

Your system should now be set up for carbonOS and GDE development. If
you are not running carbonOS, you'll need to install these packages to
compile the system:

- buildstream 1.6.1
- ostree (recent)
- cargo (recent) (if tracking)
- go (recent) (if tracking)

Dependencies marked as `(if tracking)` are only necessary if you plan on updating the packages in carbonOS
(i.e. running `tools/update` or `bst track`).

#### Deciding on your target

carbonOS is built somewhat modularly, and you need to decide which modules you
want to build. To do so, you'll need to answer a few questions:

> What device are you targetting? (`IMAGE`)

Device image definitions live in `elements/images/`, and they include device-specific
packages and configuration. Every image type in that folder has a README file that
will give you a good idea what devices that image is appropriate for, as well as
information that will help you answer the next few questions.

> What kernel do you want to use? (`KERNEL`)

Kernel definitions live in `elements/kernels`. Refer to your target device's README
file for guidance on what kernel to pick

> What architecture are you targetting? (`ARCH`)

This really depends on your needs. Your target device's README file will list which
CPU architectures are supported by that image.

> What variant of carbonOS do you want to use? (`VARIANT`)

carbonOS has a few basic variants that can be selected depending on your needs (desktop, devel, mobile).
They differ in available packages and in configuration.

#### Building

Now you can start by building the core OS. The core makes up most of the system;
it is only missing device-specific packages, a kernel, and an initramfs. To build the core, run:

```bash
$ bst build -o arch=ARCH variants/VARIANT/files.bst
[...]
```

This will take a few hours, or longer depending on your hardware. Next, you'll
need to build a kernel+initramfs package. This makes the core you just built
bootable. Simply run:

```bash
$ bst build --max-jobs N -o arch=ARCH kernels/KERNEL/all.bst
[...]
```

Replace N with the number of CPU threads you want to allocate to this build.
Since it is only building a kernel, it is better to allocate all of the CPU to
one package instead of letting multiple packages build at once as we did with
the core. To find out how many threads your CPU has available, run `nproc`.

Next, you'll build a release image of the OS that you can flash onto an install
medium or directly onto the device, depending on your needs (and on the device's
properties. Please see `elements/images/DEVICE/README.md` for details about the
generated files and where they should go). Simply run:

```bash
$ bst build images/DEVICE/image.bst
[...]
$ tools/checkout images/DEVICE/image.bst
[...]
$ ls result/
carbonOS-2020.5-DEVICE.img
```

#### Publishing

[TODO: Finish this with uploading the files to an ostree remote]

[TODO: Figure out how to allow the builder to configure where their remote is]

#### Building everything

If you are redistributing carbonOS and want to build all available cores,
kernels, and images, you can do so with this command:

```bash
$ bst build -o arch=ARCH tools/everything.bst
```

Then, you can publish these binaries with:

```bash
TODO
```

## Installation instructions

**PSA: DO NOT DO THIS ON YOU MAIN MACHINE**. carbonOS is **PRE-ALPHA** quality software
and it will NOT be usable as a daily-driver OS just yet.

carbonOS's installer ISO file can be found [here](https://carbonos-iso.s3.us-east-2.amazonaws.com/carbonOS-2020.1-x86_64.iso). It's only
recommended that you run this on physical hardware or using GNOME Boxes as a VM.
Other VM programs are untested and are not likely to work. To install carbonOS:

- Boot from the installer media
  - If on physical hardware, write the ISO to a flash drive (you can use [balenaEtcher](https://etcher.io) for this) and boot the computer from that
  - If using GNOME Boxes, follow the instructions under "Setting up GNOME Boxes" before continuing
- Follow the on-screen setup instructions. You do not need to set a root password
- Once at the login screen: username is `live` and there is no password
- From here, you can try out carbonOS. If you decide to install it, continue:
- Open "Disks" and figure out which disk you want to install to
- Select the disk, then find the /dev subheading at the top-center of the window. Remember this value
- Open "Terminal" and type `doas os-installer`. Just leave the password blank when asked
- Type in the /dev value from earlier at the prompt
- Type in f2fs (or ext4 if you know what you're doing)
- Wait for the installer to finish
- Shut down the system (either press your hardware's power button or search for "shutdown" in the app launcher)
- Remove the installer media
    - If on physical hardware, remove the flash drive
    - If using Boxes, right click on the VM, go to properties, then "Devices & Shares", and then press "Remove" next to "CD/DVD"
- Reboot and follow setup instructions. You can skip setting a root password
- Sign in with the user account you just created
- If necessary, edit the display config file to change your display scale
	- Hit Super+R to open the run prompt
	- Type `gnome-text-editor ~/.config/wayfire.ini`
	- Remove the `#` from the `#scale=1.6` line and change the number as you see fit
	- If changing scale doesn't work, try replacing `eDP-1` on the line above with one of these other common values: `HDMI-A-1`, `DP-1`, `HDMI-A-2`, `Virtual-1`, `HDMI-B-1`, `HDMI-B-2`
- Connect to WiFi
	- In terminal, run `nmcli d wifi` for a list of available networks
	- The run `nmcli d wifi connect --ask` and follow the prompts
- Congrats! You've successfully set up carbonOS!

Setting up GNOME Boxes:

- Create a new virtual machine
- Select "Operating System Image File"
- Choose the carbonOS installer iso you downloaded
- Change the template from "Unknown OS" to "GNOME OS"
- Follow the on-screen instructions to finish creating the virtual machine
- Boxes will start booting the image

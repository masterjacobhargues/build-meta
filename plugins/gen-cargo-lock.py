"""
Generates a Cargo.lock file for projects that refuse to
ship one. If a project does not have a Cargo.lock file,
stage this source before staging the `cargo` source.
"""

import os
from buildstream import Source, SourceError, Consistency
from buildstream import utils

class GenCargoLockSource(Source):
    BST_REQUIRED_VERSION_MAJOR = 1
    BST_REQUIRED_VERSION_MINOR = 3
    BST_REQUIRES_PREVIOUS_SOURCES_TRACK = True

    def configure(self, node):
        self.ref = self.node_get_member(node, str, "ref", None)
        self.node_validate(node, Source.COMMON_CONFIG_KEYS + ['ref'])

    def preflight(self):
        self.host_cargo = utils.get_host_tool("cargo")

    def get_unique_key(self):
        return self.ref

    def load_ref(self, node):
        self.ref = self.node_get_member(node, str, "ref", None)

    def get_ref(self):
        return self.ref

    def set_ref(self, ref, node):
        node['ref'] = self.ref = ref

    def get_consistency(self):
        if self.ref is None:
            return Consistency.INCONSISTENT

        check = os.path.join(self.get_mirror_directory(), self.ref)
        if os.path.exists(check):
            return Consistency.CACHED

        return Consistency.RESOLVED

    def track(self, previous_sources_dir):
        new_ref = utils.sha256sum(os.path.join(previous_sources_dir, "Cargo.toml"))
        target = os.path.join(self.get_mirror_directory(), new_ref)

        with self.timed_activity("Generating Cargo.lock"):
            command = [self.host_cargo, "update"]
            self.call(command, cwd=previous_sources_dir, fail="Error calling `cargo update`")
            utils.safe_copy(os.path.join(previous_sources_dir, "Cargo.lock"), target)

        return new_ref

    def fetch(self):
        pass

    def stage(self, directory):
        source = os.path.join(self.get_mirror_directory(), self.ref)
        target = os.path.join(directory, "Cargo.lock")
        utils.safe_copy(source, target)

# Entry point
def setup():
    return GenCargoLockSource

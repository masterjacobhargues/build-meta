"""
Downloads all the dependencies of a go project
"""

import os
from buildstream import Source, SourceError, Consistency
from buildstream import utils

class GoVendorSource(Source):
    BST_REQUIRED_VERSION_MAJOR = 1
    BST_REQUIRED_VERSION_MINOR = 3
    BST_REQUIRES_PREVIOUS_SOURCES_TRACK = True

    def configure(self, node):
        self.ref = self.node_get_member(node, str, "ref", None)
        self.node_validate(node, Source.COMMON_CONFIG_KEYS + ['ref'])

    def preflight(self):
        self.host_go = utils.get_host_tool("go")

    def get_unique_key(self):
        return self.ref

    def load_ref(self, node):
        self.ref = self.node_get_member(node, str, "ref", None)

    def get_ref(self):
        return self.ref

    def set_ref(self, ref, node):
        node['ref'] = self.ref = ref

    def get_consistency(self):
        if self.ref is None:
            return Consistency.INCONSISTENT

        check = os.path.join(self.get_vendor_dir(), "modules.txt")
        if os.path.exists(check):
            return Consistency.CACHED

        return Consistency.RESOLVED

    def track(self, previous_sources_dir):
        go_sum = os.path.join(previous_sources_dir, "go.sum")
        new_ref = utils.sha256sum(go_sum)

        target = os.path.join(self.get_mirror_directory(), new_ref)
        os.makedirs(target, exist_ok=True)

        with self.timed_activity("govendor: Downloading dependencies"):
            command = [self.host_go, "mod", "vendor"]
            self.call(command, cwd=previous_sources_dir, fail="Error calling `go mod vendor`")
            utils.copy_files(os.path.join(previous_sources_dir, "vendor"), target)

        return new_ref

    def fetch(self):
        pass

    def stage(self, directory):
        target = os.path.join(directory, "vendor")
        with self.timed_activity("govendor: Staging sources"):
            utils.copy_files(self.get_vendor_dir(), target)

    def get_vendor_dir(self):
        return os.path.join(self.get_mirror_directory(), self.ref)

# Entry point
def setup():
    return GoVendorSource

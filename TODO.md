# carbonOS TODO List

This file describes carbonOS's roadmap for the next few releases. It is being
continuously updated. Once carbonOS switches to different infrastructure, this
file will be replaced with a proper task management system (eg Trello, etc).

---

# Implemented things to test

# Pending issues / WIP

- flatpak cannot handle empty /var/lib/flatpak
    - Once fixed, re-enable flatpak lines in files/systemd/tmpfiles.conf
    - https://github.com/flatpak/flatpak/issues/4111
- ffi static trampolines broke gjs and pygobject
    - https://github.com/libffi/libffi/pull/647
    - https://gitlab.gnome.org/GNOME/gobject-introspection/-/merge_requests/283
    - https://gitlab.gnome.org/GNOME/gjs/-/issues/428
    - Once resolved, remove --disable-exec-static-tramp from libffi
- WebKitGTK build broken by cmake 3.21.0
    - https://bugs.webkit.org/show_bug.cgi?id=228267
    - Once fixed, remove patch from webkitgtk
- LLVM build broken by cmake 3.21.0
    - https://bugs.llvm.org/show_bug.cgi?id=51115
    - Once fixed, remove workaround in llvm
- g-c-c ignores -dark logo variant
    - https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/1415
    - Once fixed, remove workaround in branding

# Uninvestigated Bugs

- Repository not writable error in os-updated
- GDE race condition of new notif comes in right as old was being dismissed in popup
- Regular users can't look at their own logs
- "App Settings" menu item doesn't work in gde-panel

---

# carbonOS 2021.1 (Alpha)

### Graphical Session

- Re-enable auto-timezone
- Package libgnomekbd for the keyboard layout preview window for g-c-c
- Package squeekboard & add custom systemd service for it
- Figure out sane defaults for input settings
- Package gnome-user-docs

### System

- Port to openprinting cups
    - Fixup the systemd preset
    - Drop cups-lpd
    - Install utils/cups-browsed.service for systemd
- Don't track RC versions of packages
    - meson
    - Go through recent history and find everything that ends in -rc or -alpha or so in project.refs
- Enable btrfs compression on ~/.local/share/flatpak, ~/.cache (by using user-tmpfiles.d)
- Properly bootstrap vala so we can track it from git
    - Make g-ir a runtime dep of valac
- Package bash-completion (https://github.com/scop/bash-completion)
- Get rid of /usr/share/pkgconfig
- Rename generic kernel -> mainline
- Open feature request @ buildstream to schedule jobs such that they'll use all of the available cores at once
    - Maybe an option like `config.monopolize-cpu: true`, and then buildstream will schedule so that the build uses
      all of the available threads at once. That way, most packages can be built in parallel, but the big fat ones can use
      all of the available CPU

---

# carbonOS 2021.2 (Beta)

##### Graphical Session

- Wrap wayfire in a bash script that sets environment variables
    - Use in systemd
    - Use in lightdm greeter config
- Fixup broken spacing in gde-panel's control center with only 2 items
- Update gde-gnome-bridge's output display name logic
- gde-panel: Don't try to launch bluetooth on unsupported hardware
- Implement network manager support in gde-dialogs & drop nm-applet
- Make wayfire use rtkit
- Folders in gde-panel launcher
- Carefully consider which codecs to include/remove from gstreamer
- Try to minimize or even drop ffmpeg (we're mostly gonna rely on gstreamer)
- Finish gde-gnome-bridge
- "What's new"
    - Yelp page of the changelog (translated, hopefully)
    - On session start, compare gsetting to /etc/os-release's VERSION
    - If /etc is newer, launch yelp. Else do nothing
    - Update the gsetting
- Package vulkan (and update gst-bad, gtk4, pipewire)
    - Don't enable vulkan in gtk4 for now: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1220
- Hook udisks2 up to gde (unmount applet, unlock volume prompts, mount/unmount sounds)
- Package baobab (disk usage)
- Package gnome-fonts
- Package gnome-characters
- Package wayvnc?? (gnome-remote-desktop just uses pipewire so we can probably just do that?)
- Package gnome-music and gnome-videos (or install as flatpaks?)

##### System

- Package the VPNs for g-c-c
- Make bash look for ~/.bashrc and ~/.bash_profile in ~/.config. Move to /etc/bash.bashrc for system-wide bashrc & don't manually source @ runtime. See arch for reference
- Move packages out of early build environment if appropriate/necessary:
    - openssl
    - popt
    - bzip2
    - lua
    - file
- Package libfprint/fprintd (https://gitlab.freedesktop.org/libfprint)
- Package boltd (thunderbolt)
- Maybe pick a better shell (possibly default):
    - Pick fish shell (https://fishshell.com) or zsh (https://zsh.sourceforge.io/)
    - If zsh: Decide if this is a devel or base image thing
    - If fish: We need to port all the scripts in /etc/profile.d to be fish-compatible
    - We _could_ go with both: fish on desktop and zsh on devel image. However, sounds like a lot of configuration options to worry about
- Come up with a better way to get cached bootstrap
    - Maybe just pull down fdo's bootstrap base and call it a day? Are there enough packages in there?
    - Split the bootstrap into its own project, update it less regularly, etc?
- Downloadable debuginfo - filter out /usr/debug onto a server and then download files as-needed. Maybe by OS version? (https://repos.carbon.sh/debuginfo/2021.2/pkgs-gtk4.tar.gz)
- Package rtkit
    - For pipewire
    - For wayfire
- Build our own rustc
- Fix git submodule warnings
- Enable gnome-terminal search provider
- Delete libtool files for all autoconf builds (https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/master/include/_private/autotools-conf.yml)
- Compile python modules (see pkgs/gobject-introspection for guidance on that)
- Rebase the kernel config on arch's latest config
- Mark boot successful if user session has run for a few minutes
- Download flatpaks from flathub, then bake them into the installer image OOTB
- Split libsoup into `libsoup2` and `libsoup` (which will be v3+)
- Split x86_64 into x86_64-v3 and x86_64
- Disable the editor in systemd-boot by default
- Switch from fdo-stripper to find-debuginfo:
  - Make MAXJOBS=%{max-jobs} and put in environment-nocache project-wide
    - Remove references to it from nss and mozjs
  - Make debugedit runtime-depend on cpio & dwz
  - Package elfutils,dwz,cpio,debugedit in bootstrap/ to unify stripping config
    - Figure out what compression types need to go into elfutils
  - Drop fdo-stripper.bst
  - Replace strip-binaries w/
    - mkdir _debuginfo
    - RPM_BUILD_ROOT=%{install-root} RPM_BUILD_DIR=%{build-root}
    - RPM_PACKAGE_NAME=$(echo "%{element-name}" | sed -e "s|\/|-|g" -re "s|(-all)?\.bst||g" -re "s|pkgs-||")
    - RPM_PACKAGE_VERSION=%{project-name} RPM_PACKAGE_RELEASE=%{version} RPM_ARCH=%{arch}
    - find-debuginfo --run-dwz -m -j ${MAXJOBS} _debuginfo/
  - Control --run-dwz above w/ the optimize-debug variable. Drop the compress-debug variable. Change type to boolean
  - Extend debugedit:
    - Replace the RPM_* env vars
        - Should auto-fallback to the RPM env vars
        - Add a --buildroot= flag to replace RPM_BUILD_ROOT
        - Add a --srcdir= flag to replace RPM_BUILD_DIR
        - Add a --pkgname= flag to replace "${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}-${RPM_PACKAGE_RELEASE}.${RPM_ARCH}"
        - https://sourceware.org/bugzilla/show_bug.cgi?id=27637
    - Use DWARF 5
        - Add a --dwarf-5 flag to pass through to dwz
        - Make it a compile-time check? Open a bug report?
    - Uncomment the debug strip-rule in include/config/strip-rules.yml


##### Compatability layers (MAY NEED SOME REDESIGN)

- Based around flatpak runtimes
- Shared GUI component:
    - "This program was not designed to run on your system, but carbonOS can try
    to run it with a compatiblity layer. Install the necessary files?"
    - This program was not designed to run on your system, but carbonOS can try to run it in
    compatability mode. The following components are missing from your system: Microsoft Windows™ Runtime (506 mb). Install them?
    - Dropdown of "more details" showing exactly what it wants to install, from
    where, and what it's trying to run
    - If user hits install, progress bar of installing the runtime. Then close
    and run the program immediately
- Files live in `/usr/libexec/compat/wrap-FMT`
    - FMT: jar, appimage, exe, x86 (32-bit program), apk, rpm, deb, aarch64, [...]
- Wrapper executes the GUI to install the runtime if necessary, and then
executes the program from within the runtime
- Use binfmt.d to wrap the incompatible program with the file from libexec

---

# At some point

### Graphical

- Boot animation for vendors: we'd want some way to configure vendor installs to have a "Powered by carbonOS"
 watermark?
- Crash handler GUI (???) Integrate into systemd-coredump?

### Misc.

- Replace '' with "" throughout the codebase
- Replace our horrible build-dir mangling for autotools w/ command-subdir + conf-cmd combo
- Does gamemode actually work?
- Does fwupd actually work?
- Google safe-browsing API key
- Make sure ssh root password login is disabled
- Move nano's EDITOR=nano into its own profile.sh script
- Compress firmware
- Upgrade to openssl 3.0
- Package SANE (scanning) and simple-scan (?)
- Package intel's thermald
- Package libfprint
- Package pykeepass
- Implement systemd-homed once somewhat supported upstream
    - Drop shadow & related cruft
- Generate yelp documentation file for all the copyrights
- Remove IWD workaround patch once it is fixed upstream
- Package gnome-photos when it replaces eog
- User/version tally system
- Package/Set up apparmor for enhanced sandboxing and security
- Upgrade to fuse3 where possible
    - Not possible: ostree, ntfs-3g (check these)
- Put git in the bootstrap and remove some git-related cruft from the early build environment
- Enable LTO system-wide (right now we're just doing meson)
- Wayland-only: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/454/diffs
- https://systemd.io/COREDUMP_PACKAGE_METADATA/
- Enable -Dauto_features=enabled in meson
- Automated tests w/ openqa: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1251

### Look into

- TPM support?
    - fwupd
    - where else?
- Fido security tokens (openssh, greeter)?
- Should we build/include man pages & man itself? It seems that a bunch of stuff assumes man is functional
	- If we do include man, dont include man books about C code and stuff like that. Just config files & binaries in /usr/bin
- libphonenumber (just needs protobuf; evolution-data-server)?
- Postscript support (evince)?
- libopenraw?
- Packages that are known to still require GDK-X11:
    - gnome-settings-daemon
    - gcr
    - libcanberra
- Bonsai
- cloudproviders
- cloudprint?
- CUPS from OpenPrinting and not from Apple upstream??
- Package neard (nfc)?
- Package nice/extra thumbnailers
    - https://gitlab.gnome.org/ZanderBrown/gnome-source-thumbnailer
    - https://github.com/exe-thumbnailer/exe-thumbnailer
- Bottles app for windows compat (build it into the OS somehow?)
- Risc-v support https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/merge_requests/4532/diffs#fff54af28a9e2050dcbdf50aca48d96fdda70857
- https://fleet-commander.org/
- Extensions as layers in os-updated OR as overlays via systemd-sysext

### /usr/libexec/esp-update (NEEDS SOME REDESIGN)

- Will be used to keep bootloader, shim, and ext4 FS driver up-to-date. Potentially can be used with fwupd also (I'll have to do some research into how fwupd updates itself today)
- systemd should be configured to mount the ESP read-only on /efi. Nothing but this tool should be touching the ESP post-install.

- Reads configuration file: /etc/esp-update.conf. Format: `[FROM] [TO] [VARIABLE]`. `FROM` is an absolute path relative to the OS root,
  and `TO` is an absolute path relative to the ESP. `VARIABLE` is the name of an EFI variable into which to put `TO`, and is optional.
  If you do not have a variable value to set, `VARIABLE` should equal `-`. Setting `VARIABLE` updates the behavior of the tool to
  be more atomic.
- For example: `/usr/lib/systemd/boot/efi/systemd-bootx64.efi /EFI/BOOT/BOOTX64.EFI` will keep systemd-boot up to date.
- Enters a mount namespace and remounts /efi rw
- Compares the checksum of the existing file (if it exists) and the potentially new file. If they don't match, continue with that file
- Copy the file into the ESP into the correct location (PATH from here on out) with a .staged file extension
- `sync` the file into place
- Try to glnx_renameat2_exchange(PATH.staged -> PATH), and if ENOENT do glnx_renameat2_noreplace(PATH.staged -> PATH)
- `sync` the renames into place
- If it still exists, delete PATH.staged
- `sync`

